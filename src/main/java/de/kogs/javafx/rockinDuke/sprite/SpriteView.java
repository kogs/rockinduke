/**
 *
 */
package de.kogs.javafx.rockinDuke.sprite;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class SpriteView extends ImageView {

	private SpriteSheet sheet;
	protected int animationState = 0;
	protected int maxState = 0;
	
	
	private int spriteWidth;
	private int spriteHeight;
	
	public SpriteView(InputStream image)
			 {
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
		try {
			sheet = new SpriteSheet(ImageIO.read(image));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public void show(int x, int y, int w, int h) {
		setImage(sheet.crop(x, y, w, h));
	}
	
	public void update(){
		animationState++;
	}

}
