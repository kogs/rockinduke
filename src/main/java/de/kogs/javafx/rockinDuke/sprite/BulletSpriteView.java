/**
 *
 */
package de.kogs.javafx.rockinDuke.sprite;



/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class BulletSpriteView extends SpriteView {
	
	/**
	 * @param image
	 */
	public BulletSpriteView () {
		super(BulletSpriteView.class.getResourceAsStream("/images/Items/Bullet2.png"));
		
	}
	
	/* (non-Javadoc)
	 * @see de.kogs.javafx.rockinDuke.sprite.SpriteView#update()
	 */
	@Override
	public void update() {
		super.update();
		
		if (animationState == 1) {
			show(0, 0, 75, 70);
		} else if (animationState == 4) {
			show(75, 0, 75, 70);
			
		} else if (animationState == 8) {
			show(150, 0, 75, 70);

//		} else if (animationState == 3) {
//			
//		} else if (animationState == 4) {
//			
//		} else if (animationState == 5) {
//			
//		} else if (animationState == 6) {
//			
		} else if (animationState >= 12) {
			animationState = 0;
		}
		
	}

}
