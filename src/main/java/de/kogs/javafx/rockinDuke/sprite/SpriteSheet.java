package de.kogs.javafx.rockinDuke.sprite;

import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public class SpriteSheet {

	private BufferedImage sheet;

	public SpriteSheet(BufferedImage sheet) {
		this.sheet = sheet;
	}

	public Image crop(int x, int y, int width, int height) {
		BufferedImage subimage = sheet.getSubimage(x, y, width, height);
		//TODO cache Images
		return SwingFXUtils.toFXImage(subimage,
				new WritableImage(width, height));
	}

}
