package de.kogs.javafx.rockinDuke.sprite;

import de.kogs.javafx.rockinDuke.actors.Duke;

public class Guitar extends SpriteView {

	private boolean shooting = false;
	private Duke duke;

	public Guitar(Duke duke) {
		super(Guitar.class.getResourceAsStream("/images/Items/Guitar.png"));
		this.duke = duke;
		defaultState();
	}

	public void defaultState() {
		shooting = false;
	}

	public void shootAnimation() {
		if (!shooting) {
			animationState = 0;
		}
		shooting = true;
	}

	@Override
	public void update() {

		if (shooting) {
			if (animationState == 0) {
				show(47, 0, 47, 75);
			} else if (animationState == 2) {
				show(94, 0, 47, 75);
			} else if (animationState == 4) {
				show(141, 0, 47, 75);
			} else if (animationState == 6) {
				show(188, 0, 47, 75);
			} else if (animationState == 8) {
				show(235, 0, 47, 75);
			} else if (animationState == 10) {
				show(282, 0, 47, 75);
			} else if (animationState == 12) {
				show(329, 0, 47, 75);
				shoot();
				shooting = false;
			}
		} else {
			show(0, 0, 47, 75);
		}

		super.update();

	}

	private void shoot() {
		duke.shoot();
	}

}
