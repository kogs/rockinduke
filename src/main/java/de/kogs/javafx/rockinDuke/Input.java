/**
 *
 */
package de.kogs.javafx.rockinDuke;

import de.kogs.javafx.rockinDuke.geom.Vec2d;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;

import javafx.scene.input.MouseButton;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class Input {

	private static List<KeyCode> downCodes = new ArrayList<>();
	private static Vec2d mousePos = new Vec2d(0, 0);
	private static List<MouseButton> mouseDown = new ArrayList<>();

	public static void initForScene(Scene scene) {

		scene.setOnKeyPressed((event) -> {

			if (!downCodes.contains(event.getCode())) {
				downCodes.add(event.getCode());
			}

		});
		scene.setOnKeyReleased((event) -> {

			downCodes.remove(event.getCode());
		});

		scene.setOnMouseMoved((event) -> {
			mousePos.x = event.getX();
			mousePos.y = event.getY();
		});

		scene.setOnMouseDragged((event) -> {
			mousePos.x = event.getX();
			mousePos.y = event.getY();
		});

		scene.setOnMousePressed((event) -> {
			if (!mouseDown.contains(event.getButton())) {
				mouseDown.add(event.getButton());
			}
		});
		scene.setOnMouseReleased((event) -> {
			mouseDown.remove(event.getButton());
		});

	}

	public static boolean isKeyDown(KeyCode code) {
		return downCodes.contains(code);
	}
	
	public static boolean isMouseDown(MouseButton button){
		return mouseDown.contains(button);
	}
	
	public static Vec2d getMousePos() {
		return new Vec2d(mousePos);
	}

}
