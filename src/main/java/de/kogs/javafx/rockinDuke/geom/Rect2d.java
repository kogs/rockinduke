/**
 *
 */
package de.kogs.javafx.rockinDuke.geom;

import javafx.geometry.Rectangle2D;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class Rect2d extends Rectangle2D {
	
	/**
	 * @param minX
	 * @param minY
	 * @param width
	 * @param height
	 */
	public Rect2d (double minX, double minY, double width, double height) {
		super(minX, minY, width, height);
	}
	
	public double getCenterX() {
		return getMinX() + getWidth() / 2;
	}
	
	public double getCenterY() {
		return getMinY() + getHeight() / 2;
	}
	
}
