/**
 *
 */
package de.kogs.javafx.rockinDuke;

import de.kogs.javafx.rockinDuke.actors.Actor;
import de.kogs.javafx.rockinDuke.geom.Vec2d;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class World extends Pane {
	
	private List<Actor> actors = new ArrayList<Actor>();
	public Vec2d gravity = new Vec2d(0, 0.1);
	public double airFrictionMultiplier = 0.99;
	public double groundFrictionMultiplier = 0.85;
	
	public void update(long now) {
		List<Actor> tmpActors = new ArrayList<>(actors);
		for (Actor actor : tmpActors) {
			actor.update(now);
			for (Actor ac : actors) {
				if (ac != actor) {
					if (actor.collider.intersects(ac.collider)) {
						if (ac.collisionTriggered || actor.collisionTriggered) {
							
						} else {

						}
					}
				}
			}
		}
	}

	public void addActor(Actor actor){
		actors.add(actor);
		actor.setWorld(this);
		getChildren().add(actor.getNode());
	}
	
	/**
	 * @param actor
	 */
	public void removeActor(Actor actor) {
		actors.remove(actor);
		actor.setWorld(null);
		getChildren().remove(actor.getNode());
	}
}
