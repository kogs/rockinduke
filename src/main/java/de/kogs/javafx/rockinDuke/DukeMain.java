/**
 *
 */
package de.kogs.javafx.rockinDuke;

import de.kogs.javafx.rockinDuke.actors.Actor;
import de.kogs.javafx.rockinDuke.actors.Ball;
import de.kogs.javafx.rockinDuke.actors.Duke;
import de.kogs.javafx.rockinDuke.geom.Vec2d;

import java.util.Random;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class DukeMain extends Game {

	public static void main(String[] args) {
		System.setProperty("javafx.animation.fullspeed", "true");
		Game.launchGame(DukeMain.class);
	}

	/* (non-Javadoc)
	 * @see de.kogs.javafx.rockinDuke.Game#start()
	 */
	@Override
	protected void start() {
		Actor duke = new Duke();
		world.addActor(duke);
		
		Random rand = new Random();
		
		for (int i = 0; i < 2; i++) {
			Actor ball = new Ball(rand.nextInt(20) + 10);
			world.addActor(ball);
			ball.teleport(new Vec2d(rand.nextInt((int) world.getWidth()), 10));

			
		}

	}


}
