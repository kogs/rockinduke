/**
 *
 */
package de.kogs.javafx.rockinDuke;

import javafx.animation.AnimationTimer;
import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public abstract class Game extends Application {
	
	protected World world;
	protected Stage stage;
	protected double deltaTime = 0;
	protected int fps = 0;
	private int impl_fps = 0;

	
	public static void launchGame(Class<? extends Game> clazz) {
		System.setProperty("javafx.animation.fullspeed", "true");
		Application.launch(clazz, new String[0]);
	}

	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		primaryStage.setResizable(false);


		world = new World();
		
		Scene scene = new Scene(world, 1000, 800);
		Input.initForScene(scene);
		primaryStage.setScene(scene);


		PauseTransition fpsCounter = new PauseTransition(Duration.seconds(1));
		fpsCounter.setOnFinished((event) -> {
			fps = impl_fps;
			impl_fps = 0;
			System.out.println(fps);
			fpsCounter.play();
		});
		fpsCounter.play();

		AnimationTimer timer = new AnimationTimer() {
			double lastFrame = 0;

			@Override
			public void handle(long now) {
				deltaTime = now - lastFrame;
				lastFrame = now;
				impl_fps++;
				update(now);
			}
		};

		start();
		timer.start();
		primaryStage.show();
	}
	

	private void update(long now) {
		world.update(now);
	}
	
	
	
	
	protected abstract void start();
	
	
	
}
