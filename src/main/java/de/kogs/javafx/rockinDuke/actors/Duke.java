/**
 *
 */
package de.kogs.javafx.rockinDuke.actors;

import de.kogs.javafx.rockinDuke.Input;
import de.kogs.javafx.rockinDuke.geom.Vec2d;
import de.kogs.javafx.rockinDuke.sprite.Guitar;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.transform.Rotate;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class Duke extends Actor {
	
	private Guitar guitar;
	private Rotate rotate;

	/**
	 * @param node
	 */
	public Duke () {
		super(new Group(), 47, 75);
		
		Group root = (Group) getNode();
		root.getChildren().add(new ImageView(new Image(Duke.class.getResourceAsStream("/images/Duke/DukeBody.png"))));
		guitar = new Guitar(this);
		spriteViews.add(guitar);
		root.getChildren().add(guitar);
		guitar.setLayoutX(47 / 2);
		guitar.setLayoutY(-10);
		
		rotate = new Rotate(0, 47 / 2, 60);
		guitar.getTransforms().add(rotate);

		bouncines = 0.01;
		mass = 1.5;

	}
	
	/* (non-Javadoc)
	 * @see de.kogs.javafx.rockinDuke.Actor#update()
	 */
	@Override
	public void update(long now) {
		if (Input.isKeyDown(KeyCode.A)) {
			velocity.x -= .5;
		} else if (Input.isKeyDown(KeyCode.D)) {
			velocity.x += .5;
		}
		
		if (Input.isKeyDown(KeyCode.SPACE) && grounded) {
			velocity.y -= 2.5;
		}
		if(Input.isMouseDown(MouseButton.PRIMARY)){
			guitar.shootAnimation();
		}

		super.update(now);
		
		// Look direction
		Vec2d mousePos = Input.getMousePos();

		mousePos.remove(posAsVec());
		
		rotate.setAngle(mousePos.toAngel() + 90);
	}

	public void shoot() {
		Bullet bullet = new Bullet();
		
		double angle = rotate.getAngle()-90;
		double x = Math.cos(Math.PI * angle / 180);
		double y = Math.sin(Math.PI * angle / 180);
		bullet.teleport(new Vec2d(collider.getCenterX() - bullet.collider.getWidth() / 2, collider.getCenterY()
				- bullet.collider.getHeight() / 2));
		Vec2d v = new Vec2d(x * 20, y * 20);
		bullet.velocity.add(v);
		world.addActor(bullet);
	}

}
