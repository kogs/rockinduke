/**
 *
 */
package de.kogs.javafx.rockinDuke.actors;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 *
 */
public class Ball extends Actor {
	
	/**
	 * @param node
	 * @param width
	 * @param height
	 */
	public Ball (double size) {
		super(new Circle(size / 2, Color.BLUE), size, size);
		bouncines = 0.8;
	}
	


}
