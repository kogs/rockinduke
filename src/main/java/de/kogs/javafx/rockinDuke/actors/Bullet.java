package de.kogs.javafx.rockinDuke.actors;

import de.kogs.javafx.rockinDuke.sprite.BulletSpriteView;
import javafx.scene.Group;
import javafx.util.Duration;

public class Bullet extends Actor {

	public Bullet() {
		super(new Group(), 75, 70);
		
		BulletSpriteView bulletSprite = new BulletSpriteView();

		spriteViews.add(bulletSprite);

		((Group) getNode()).getChildren().add(bulletSprite);
		useGravity = true;
		mass = 0.3;
		bouncines = 0.2;
		collisionTriggered = true;
		destroy(Duration.seconds(3));
	}
	
	/* (non-Javadoc)
	 * @see de.kogs.javafx.rockinDuke.actors.Actor#update()
	 */
	@Override
	public void update(long now) {
		super.update(now);
		getNode().setRotate(velocity.toAngel() + 270);
		
	}

}
