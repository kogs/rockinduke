/**
 *
 */
package de.kogs.javafx.rockinDuke.actors;

import de.kogs.javafx.rockinDuke.World;
import de.kogs.javafx.rockinDuke.geom.Rect2d;
import de.kogs.javafx.rockinDuke.geom.Vec2d;
import de.kogs.javafx.rockinDuke.sprite.SpriteView;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:marcel.vogel@proemion.com">mv1015</a>
 */
public class Actor {

	public Vec2d velocity = new Vec2d(0, 0);
	protected double mass = 1;
	public Rect2d collider;
	public boolean collisionTriggered = false;

	protected boolean useGravity = true;

	private Node node;
	protected World world;

	protected double bouncines = 0;

	protected boolean grounded = false;

	protected List<SpriteView> spriteViews = new ArrayList<SpriteView>();

	private long selfDestory = -1;

	/**
	 * 
	 */
	public Actor (Node node, double width, double height) {
		this.node = node;
		collider = new Rect2d(0, 0, width, height);

//		PauseTransition pause = new PauseTransition(Duration.millis(10));
//		pause.setOnFinished((event) -> {
//
//			for (SpriteView spriteView : spriteViews) {
//				spriteView.update();
//			}
//
//			pause.play();
//		});
//		pause.play();
	}

	public void update(long now) {
		// physics stuff

		if (useGravity) {
			velocity.x += world.gravity.x * mass;
			velocity.y += world.gravity.y * mass;
		}

		// World Bounds

		// Bottom
		if (collider.intersects(0, world.getHeight(), world.getWidth(), 10)) {
			if (velocity.y > 0) {
				velocity.y = velocity.y * -bouncines;
			}
			grounded = true;
		} else {
			grounded = false;
		}

		// TOP
		if (collider.intersects(-10, 0, world.getWidth(), 10)) {
			if (velocity.y < 0) {
				velocity.y = velocity.y * -bouncines;
			}
		}

		// Left
		if (collider.intersects(-10, 0, 10, world.getHeight())) {
			if (velocity.x < 0) {
				velocity.x = velocity.x * -bouncines;
			}

			// Right

		} else if (collider.intersects(world.getWidth(), 0, 10, world.getHeight())) {
			if (velocity.x > 0) {
				velocity.x = velocity.x * -bouncines;

			}
		}
		//
		
		if (grounded) {
			velocity.x *= world.groundFrictionMultiplier;
		} else {
			velocity.x *= world.airFrictionMultiplier;
		}
		
		collider = new Rect2d(collider.getMinX() + velocity.x, collider.getMinY() + velocity.y, node.getBoundsInLocal()
				.getWidth(), node.getBoundsInLocal().getHeight());

		node.setLayoutX(collider.getMinX());
		node.setLayoutY(collider.getMinY());

		for (SpriteView spriteView : spriteViews) {
			spriteView.update();
		}

		

	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public Node getNode() {
		return node;
	}
	
	public Vec2d posAsVec() {
		return new Vec2d(collider.getCenterX(), collider.getCenterY());
	}

	public void teleport(Vec2d newPos) {
		collider = new Rect2d(newPos.x, newPos.y, node.getBoundsInLocal().getWidth(), node.getBoundsInLocal().getHeight());
		velocity.x = 0;
		velocity.y = 0;
	}

	public void addImpulse(Vec2d impulse) {
		velocity.add(impulse);
	}

	public void destroy() {
		if (world != null) {
			world.removeActor(this);
		}
	}

	/**
	 * destoryes the Actor after the given Duration
	 */
	public void destroy(Duration dur) {
		PauseTransition pause = new PauseTransition(dur);
		pause.setOnFinished(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				destroy();
			}
		});
		pause.play();
	}

}
